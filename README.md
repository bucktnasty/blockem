# Blockem

Block all undesireable people from twitter. Uses object detection and gender classification
on the user's profile picture as well.

### Install

`git clone https://gitlab.com/bucktnasty/blockem.git`

`pip install -r requirements.txt`

### Usage

Currently only the 'algorithm' is included. The crawler/autoness will be used later

`python blockem.py -u Username`

The script will create a file `blocklist.csv` that you can import via twitter.

### Tweaking

The rules/weights are set in `rules.py`

### Publish Blocklist

A pretty basic publish script `./publish` is included to quickly sort/uniq and upload your blocklist to transfer.sh
