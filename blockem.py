#!/usr/bin/python
from lib.blockemlib import *
import threading
import argparse
import queue

# handle command line arguments
ap = argparse.ArgumentParser()
ap.add_argument('-u', '--user', required=True,
	        help='twitter account @ to start blocking from')
ap.add_argument('-s', '--score', default=20,
                help= 'The score needed to block')
ap.add_argument('-ai', '--ai', dest='ai', default=True, action='store_true',
                help= 'Enable user object detection/gender classification')
ap.add_argument('-nai', '--no-ai', dest='ai', action='store_false',
                help= 'Disable user object detection/gender classification')
ap.add_argument('-t', '--threads', default=1,
                help= 'The number of threads to use, Can only use one currently if AI is enabled')
ap.add_argument('-m', '--model', default='data/gender_classification.model',
	        help='path to trained model file')
ap.add_argument('-c', '--config', default='data/yolov3.cfg',
                help= 'path to yolo config file')
ap.add_argument('-w', '--weights', default='data/yolov3.weights',
                help= 'path to yolo pre-trained weights')
ap.add_argument('-cl', '--classes', default='data/yolov3.txt',
                help= 'path to text file containing class names')
ap.add_argument('-d', '--debug', action='store_true',
                help= 'debug info')
args = ap.parse_args()

q = queue.Queue()
threads = []
done = []

def scanUser(user):
    score = 0
    max_score = int(args.score)
    acc = parseAccount(user)

    if args.debug:
        print(acc)

    if 'id' not in acc:
        if args.debug:
            print('Unable to parse account for: {}'.format(user))
        return 0

    if validators.url(acc['pfp']):
        image = url_to_image(acc['pfp'])

    #TODO: refactor this mess
    score += bio_weight(acc['bio'], args)
    score += displayName_weight(acc['displayName'], args)
    score += userName_weight(acc['userName'], args)
    score += location_weight(acc['location'], args)
    score += url_weight(acc['url'], args)

    if bool(args.ai):
        print('here')
        # do object det, as long as we were able to get the image
        if image is not None:
            objects = detect_objects(image, args)

            if args.debug:
                print("Objects Detected in Profile Picture:")
                print(objects)

            if 'person' in objects:
                gender, convidence = gender_classify(image, args)
                if args.debug:
                    print("Profile Picture shows a {}: {:.2f}%".format(gender, convidence))

                if convidence > 65 and convidence < 80:
                    score += rules['pfp'][gender]
                elif convidence >= 80 and convidence < 90:
                    score += rules['pfp'][gender]*1.3
                elif convidence >= 90:
                    score += rules['pfp'][gender]*1.5

    if acc['badges'] is not None:
        # get rid of verified losers
        if 'Verified' in acc['badges'].text:
            if args.debug:
                print("Disgusting Verified")
            score += rules['verified']
        if 'Protected' in acc['badges'].text:
            if args.debug:
                print('Private Account')
            score += rules['private']

    print("Score: {}".format(score))

    if score > max_score:
        print('Blocking {} ({})'.format(user, str(acc['id'])))

        with open('blocklist.csv', 'a+') as f:
            f.write(str(acc['id']) + '\r\n')


def worker():
    while True:
        user = q.get()
        if user is None:
            break
        scanUser(user)
        nextUsers = getUsers(user)
        for nextUser in nextUsers:
            if nextUser not in done:
                q.put(nextUser)
        done.append(user)
        q.task_done()

def main():
    num_threads = int(args.threads)
    q.put(args.user)

    for i in range(num_threads):
        t = threading.Thread(target=worker)
        t.start()
        threads.append(t)

    q.join()

    for i in range(num_threads):
        q.put(None)
    for t in threads:
        t.join()

if __name__== "__main__":
    main()
