rules = {
    'pfp': {                    # Object/Gender Detection Weights
        'female': 10,
        'male': -10,
    },
    'verified': 100,            # Weight for a verified user
    'private': 0,               # Weight for a private account
    'bio': {                    # Weights for single words (meant to be more specific)
        'she/her': 10,
        'he/him': 10,
        'xe/xer': 10,
        'they/them': 10,
        'gal': 10,
        'slut': 10,
        'whore': 10,
        'MAP': 10,
        'DSA': 10,
        'Jesus': -100,
        'furry': 10,
        'furries': 10,
        'intersectional': 10,
        'reporter': 10,
        'journalist': 10,
        'correspondent': 10,
        'fleek': 10,
        'acitivist': 10,
        'antifa': 10,
        'femme': 10,
        'lesbian': 10,
        'columnist': 10,
    },
    'bio_match': {              # Weights for a more global match in bio
        'jew': 10,
        'trans': 10,
        'patreon': 10,
        'venmo': 10,
        'cashapp': 10,
        'cardi b': 10,
        'youtube': 10,
        'snapchat': 10,
        'instagram': 10,
        'communist': 10,
        'feminist': 10,
        'husband': -10,
        'father': -10,
        'dad': -10,
        'mother': 10,
        'wife': 10,
        'mommy': 10,
        'girl': 10,
        'female': 10,
        'subscribe': 10,
    },
    'username': {               # Weights for Username (@)
        'female': 10,
    },
    'displayName': {            # Weights for Display Name ('John Smith')
        'female': 10,
    },
    'location': {               # Weights for location
        'california': 10,
    },
    'url': {                    # Weights for any urls listed
        'youtube': 10,
        'venmo': 10,
        'patreon': 10,
    }
}


