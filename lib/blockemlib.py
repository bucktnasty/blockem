import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

from bs4 import BeautifulSoup
from rules import *
import requests
import validators
import numpy as np
import cv2
import re

names = []

with open('data/names.txt', 'r') as f:
    names = [line.strip().lower() for line in f.readlines()]

"""
    bio_weight

    Calculates the weight of the user's bio

    bio (str) - The bio of the user
    args (arg parser)
    return (int) weight
"""
def bio_weight(bio, args):
    weight = 0
    words = re.sub(r'[^a-zA-Z./# ]', '', bio).split()
    # TODO: Keep these separate for a single word/phrase???
    for word in words:
        word = word.lower().replace('.', '').replace(',', '') # Clean thise up
        if word in rules['bio']:
            if args.debug:
                print('Found: {} - {}'.format(word.lower(), rules['bio'][word.lower()]))
            weight += rules['bio'][word.lower()]
    for phrase in rules['bio_match']:
        if phrase in bio:
            if args.debug:
                print('Found Phrase: {}'.format(phrase))
            weight += rules['bio_match'][phrase]
    return weight

"""
    displayName_weight

    Calculate the weight of the display name (ex: 'John Smith')

    displayName (str) - The display name of the user
    args (arg parser)
    return (int) weight
"""
def displayName_weight(displayName, args):
    displayName = displayName.lower()
    weight = 0
    for name in names:
        if name.lower() in displayName:
            if args.debug:
                print('Found Display Name: {}'.format(name))
            weight += rules['displayName']['female']
    return weight

"""
    userName_weight

    Calculate the weight of the username (@name)

    userName (str) - The username of the user (@name)
    args (arg parser)
    return (int) weight
"""
def userName_weight(userName, args):
    userName = userName.lower()
    weight = 0
    for name in names:
        if name.lower() in userName:
            if args.debug:
                print('Found User Name: {}'.format(name))
            weight += rules['username']['female']
    return weight

"""
    location_weight

    Calculate the weight of the location

    location (str) - The users location
    args (arg parser)
    return (int) weight
"""
def location_weight(location, args):
    location = location.lower()
    weight = 0
    for rule in rules['location']:
        if rule in location:
            if args.debug:
                print('Found Location: {}'.format(location))
            weight += rules['location'][rule]
    return weight


"""
    url_weight

    Calculate the weight of url listed

    url (str) - The url a user has listed
    args (arg parser)
    return (int) weight
"""
def url_weight(url, args):
    url = url.lower()
    weight = 0
    for rule in rules['url']:
        if rule in url:
            if args.debug:
                print('Found URL: {}'.format(url))
            weight += rules['url'][rule]
    return weight

"""
    parseAccount

    parse a twitter link into a dict we use for the algorithm

    user (str) - The @nameof the twitter user
    return dict {
        "pfp": (str),           #profile picture link
        "badges": (soup),       #soup structure if they have any profile badges (locked, verified)
        "displaynName": (str),  #display name of the user
        "userName": (str),      #the @ of the user (@name)
        "bio": (str),           #the bio of the user
        "location": (str),      #the location listed of the user
        "url": (str),           #the url/link listed by the user
    }
"""
def parseAccount(user):
    soup = BeautifulSoup(requests.get('https://twitter.com/{}'.format(user)).text, 'html.parser')
    twid = soup.find('div', { 'class': 'ProfileNav' })

    if twid:
        return {
            'id': twid.get('data-user-id'),
            'pfp': soup.find('img', { 'class': 'ProfileAvatar-image'}).get('src'),
            'badges': soup.find('span', { 'class': 'ProfileHeaderCard-badges' }),
            'displayName': soup.find('a', { 'class': 'ProfileHeaderCard-nameLink' }).text,
            'userName': soup.find('b', { 'class': 'u-linkComplex-target' }).string,
            'bio': soup.find('p', { 'class': 'ProfileHeaderCard-bio u-dir' }).text,
            'location': soup.find('span', { 'class': 'ProfileHeaderCard-locationText' }).text.strip(),
            'url': soup.find('span', { 'class': 'ProfileHeaderCard-urlText' }).text.strip(),
        }
    else:
        return {}

"""
    getUsers

    Grabs all the users we can find on the page.

    param user (str) - Username to get interacted/suggested users from
    return users (list)
"""
def getUsers(user):
    user = user.lower()
    soup = BeautifulSoup(requests.get('https://twitter.com/{}'.format(user)).text, 'html.parser')
    users = []
    for u in soup.find_all('span', { 'class': 'username' }):
        if len(u.text) > 1:
            if u.text.lower()[1:] != user:
                users.append(u.text[1:])
    return list(set(users))

"""
    url_to_image

    Turn an external url image into a format we can use with OpenCV

    url (str) - The url of the image
    readFlag (str) - Incase you want to read the image into something
                     different
    return image (cv2 image)
"""
def url_to_image(url, readFlag=cv2.IMREAD_COLOR):
    res = requests.get(url)
    image = np.asarray(bytearray(res.content), dtype='uint8')
    image = cv2.imdecode(image, readFlag)

    return image

"""
    detect_objects

    Uses the yolov3 object detection

    https://github.com/arunponnusamy/object-detection-opencv

    image (cv2 image) - The image to detect objects in
    return objects (list) - Returns a list of the found objects in an
                            image
"""
def detect_objects(image, args):

    def get_output_layers(net):

        layer_names = net.getLayerNames()

        output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        return output_layers


    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392

    classes = None

    with open(args.classes, 'r') as f:
        classes = [line.strip() for line in f.readlines()]

    net = cv2.dnn.readNet(args.weights, args.config)

    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)

    net.setInput(blob)

    outs = net.forward(get_output_layers(net))

    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4


    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])


    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    findings = []

    for i in indices:
        class_id = class_ids[int(i)]
        label = str(classes[class_id])
        findings.append(label)

    return findings

"""
    gender_classify

    Determines if an image contains a male or female and the
    convidence levels

    https://github.com/arunponnusamy/gender-classification

    image (cv2 image) - Image to run gender classification on
    return (label, confidence) - Returns the label (male/female)
                                 and a convidence score out of 100
"""
def gender_classify(image, args):
    from keras.preprocessing.image import img_to_array
    from keras.models import load_model

    # preprocessing
    image = cv2.resize(image, (96,96))
    image = image.astype('float') / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)

    # load pre-trained model
    model = load_model(args.model)

    # run inference on input image
    confidence = model.predict(image)[0]

    # write predicted gender and confidence on image (top-left corner)
    classes = ['male', 'female']
    idx = np.argmax(confidence)
    label = classes[idx]

    # print confidence for each class in terminal
    return (label, confidence[idx] * 100)
